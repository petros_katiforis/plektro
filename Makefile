# Returns all c files nested or not in $(1)
define collect_sources
	$(shell find $(1) -name '*.c')
endef

SOURCES = $(call collect_sources, src)
OBJECTS = $(patsubst %.c, objects/%.o, $(SOURCES))

LD_FLAGS = -lncurses

all: build run

build: $(OBJECTS)
	@echo "[Makefile] Creating the executable"
	@$(CC) $^ -o bin $(LD_FLAGS)

run:
	@./bin my_file

objects/%.o: %.c
	@# Making sure that the directory already exists before creating the object
	@mkdir -p $(dir $@)

	@echo "[Makefile] Building $@"
	@$(CC) -c $< -o $@
