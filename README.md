## Plektro, a terminal based text editor

Plektro is a simple terminal text editor with a handy and barebones file manager included. It's intended to stay simple but functional and enjoyable. It can only be run under Linux and as of now, I wouldn't suggest using it with files that you do not want to permanently lose!

The editing process will follow strict rules. You can't make any line longer than `` MAX_LINE_LEN `` characters and all tabs will be automatically converted to `` SPACES_PER_TAB `` spaces. These limitations promote consistency. Colorschemes are not included either. Regardless of me not being skilled enough yet to implement such a feature, I've already realised that I've spent far too much time trying to find the correct and perfect colorscheme and I always end up with dull and simple ones anyways. I've occasionally heared people argue that colored code is distracting and confusing.

The program is provided under the GPLv3 License and is entirely free software.

### Customization

You can customize the editor to your likings by modifying `` config.h ``. If you'd like to change some of the colors that the program uses, you can change their default values inside the `` int main() `` function. 

![Release Screenshot](/screenshots/release.png?raw=true)
