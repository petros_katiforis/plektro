/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "input.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void input_clear(input_t *input)
{
    input->content[0] = 0;
    input->cursor_x = 0;
    input->length = 0;
}

bool input_handle_input(input_t *input, int c)
{
    if (c == KEY_BACKSPACE && input->cursor_x > 0)
    {
        memmove(
            &input->content[input->cursor_x - 1],
            &input->content[input->cursor_x],
            input->length - input->cursor_x + 1
        );

        input->length--;
        input->cursor_x--;

        return true;
    }

    // Handling printable characters that should be addded to the string
    if (isprint(c) && input->length < MAX_COMMAND_LEN)
    {
        memmove(
            &input->content[input->cursor_x + 1],
            &input->content[input->cursor_x],
            input->length - input->cursor_x + 1
        );

        input->content[input->cursor_x] = c;
        input->cursor_x++;
        input->length++;

        return false;
    }


    // Handling horizontal movement
    switch (c)
    {
        case KEY_RIGHT:
            if (input->cursor_x < input->length)
            {
                input->cursor_x++;
            }

            break;

        case KEY_LEFT:
            if (input->cursor_x > 0)
            {
                input->cursor_x--;
            }

            break;
    }

    return false;
}
