/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _CONFIG_H
#define _CONFIG_H

#include <ncurses.h>

/*
 * You can tinker everything you want here to your likings
 */
#define EXIT_KEY              'q'
#define MODE_KEY              '~'
#define VISUAL_MODE_KEY       'v'
#define SAVE_KEY              's'
#define MOVE_WORD_KEY         'w'
#define MOVE_WORD_BACK_KEY    'W'
#define INSERT_BELOW_KEY      'o'
#define INSERT_UP_KEY         'O'
#define YANK_KEY              'y'
#define PASTE_BELOW_KEY       'p'
#define PASTE_UP_KEY          'P'
#define DELETE_LINE_KEY       'd'
#define CUT_LINE_KEY          'c'
#define DELETE_WORD_KEY       'f'
#define GOTO_KEY              'g'
#define CHANGE_FILE_KEY       'l'
#define DOWN_KEY              KEY_DOWN
#define UP_KEY                KEY_UP

#define ENTER_KEY             '\n'
#define TAB_KEY               '\t'
#define MAX_LINE_LEN          100
#define MAX_PATH_LEN          256
#define MAX_COMMAND_LEN       40
#define NEW_LINES_PER_RESIZE  100
#define SPACES_PER_TAB        4
#define BROWSER_WIDTH         30
#define BROWSER_HEIGHT        20

/*
 * Some useful macros are defined here
 */
#define MIN(a, b) ((a) > (b) ? (b) : (a))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define WRAP_WITH_ATTR(attr, line) \
    attron(attr);                  \
    line;                          \
    attroff(attr);                 \

/*
 * Prints the error message to the console and then exits
 * after recovering the terminal to its normal state
 */
void die(const char *message);

#endif
