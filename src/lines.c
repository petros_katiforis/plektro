/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lines.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <ncurses.h>

void line_set_content(line_t *line, const char *content)
{
    strncpy(line->content, content, MAX_LINE_LEN);
    line->length = strlen(content);
}

void line_clear(line_t *line)
{
    // Just marking the string as empty
    line->content[0] = 0;
    line->length = 0;
}

/*
 * Reallocates the array data if its end has been reached
 * Should be called before trying to insert an element on the array
 */
void lines_resize_if_needed(lines_t *lines, size_t to_be_added)
{
    if (lines->length + to_be_added < lines->capacity) return;

    // The user might be trying to add more than NEW_LINES_PER_SIZE, so we should be careful here
    size_t additional_capacity = MIN(NEW_LINES_PER_RESIZE, to_be_added);

    lines->capacity += additional_capacity;
    lines->data = realloc(lines->data, lines->capacity * sizeof(line_t));
}

/*
 * Initializes the array and fills it out based on file
 */
void lines_init_from_file(lines_t *lines, const char *file_path)
{
    // Opening the file and allocating enough space for the struct
    FILE *file = fopen(file_path, "r");
    
    if (!file)
        die("failed to open the requested file");

    lines->length = 0;
    lines->capacity = NEW_LINES_PER_RESIZE;
    lines->data = calloc(lines->capacity, sizeof(line_t));

    // Reading line data
    char buffer[MAX_LINE_LEN];

    while (fgets(buffer, MAX_LINE_LEN, file))
    {
        line_t *new_line = &lines->data[lines->length];

        line_set_content(new_line, buffer);

        // Getting rid of the new line character at the end
        new_line->content[--(new_line->length)] = 0;

        lines->length++;
        lines_resize_if_needed(lines, 1);
    }

    // Create a line if the file is completely empty
    lines->length = MAX(1, lines->length);

    fclose(file);
}

void lines_move_from_by(lines_t *lines, size_t index, int offset)
{
    // If the offset is positive, it means that we're about to insert a new line
    if (offset > 0)
    {
        lines_resize_if_needed(lines, offset);
    }

    memmove(
        &lines->data[index + offset],
        &lines->data[index],
        (lines->length - index) * sizeof(line_t)
    );

    // If we're adding lines, make sure to zero out the memory between
    // I might need to find a better way to do all this...
    if (offset > 0)
    {
        memset(&lines->data[index], 0, offset * sizeof(line_t));
    }

    lines->length += offset;
}

/*
 * Clipboard is just a linked list
 * so its implementation is quite simple
 */
void clipboard_clear(clipboard_t *clipboard)
{
    clipboard_line_t *node = clipboard->head;
    
    while (node != NULL)
    {
        clipboard_line_t *temp = node->next;
        
        // The content string is not created on the heap,
        // so we just have the free the whole thing in one call
        free(node);

        node = temp;
    }

    clipboard->head = NULL;
    clipboard->length = 0;
}

void clipboard_copy_selection(clipboard_t *clipboard, lines_t *lines, size_t start, size_t end)
{
    size_t selection_start = MIN(start, end);
    size_t selection_end = MAX(start, end);

    clipboard_clear(clipboard);

    // Filling up the list with the data
    clipboard_line_t *prev = NULL;

    for (int i = selection_start; i <= selection_end; i++)
    {
        clipboard_line_t *new_node = malloc(sizeof(clipboard_line_t));

        if (!new_node)
            die("failed to allocate space for clipboard line");

        strncpy(new_node->content, lines->data[i].content, MAX_LINE_LEN);

        // Special case in which we are creating the head
        if (i == selection_start)
        {
            clipboard->head = new_node;
        }
        else
        {
            prev->next = new_node;
        }
        
        prev = new_node;
    }

    // Making sure that no loop is being created
    // I was stuck trying to find this bug for at least two hours...
    prev->next = NULL;

    clipboard->length = selection_end - selection_start + 1;
}

void clipboard_paste_below(clipboard_t *clipboard, lines_t *lines, size_t index)
{
    lines_move_from_by(lines, index + 1, clipboard->length);

    // Iterating over the linked list and making use of the data
    size_t i = 0;

    for (clipboard_line_t *node = clipboard->head; node != NULL; node = node->next)
    {
        line_set_content(&lines->data[index + 1 + i], node->content);

        i++;
    }
}
