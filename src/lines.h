/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _LINES_H 
#define _LINES_H 

#include <stdlib.h>
#include <stdbool.h>

#include "config.h"

typedef struct
{
    char content[MAX_LINE_LEN];
    size_t length;
} line_t;

/*
 * The lines struct is just a dynamic array of editable_str_t
 * Each editable_str_t represents a single editable line
 */
typedef struct
{
    size_t capacity, length;

    line_t *data;
} lines_t;

/*
 * The clipboard will be implemented as a linked list
 * Indexing is known to be slow here, but it will never be needed
 */
typedef struct clipboard_line_t
{
    char content[MAX_LINE_LEN];
    struct clipboard_line_t *next;
} clipboard_line_t;

typedef struct
{
    size_t length;
    clipboard_line_t *head;
} clipboard_t;

void line_set_content(line_t *line, const char *content);
void line_clear(line_t *line);

void lines_init_from_file(lines_t *lines, const char  *file_path);
void lines_move_from_by(lines_t *lines, size_t index, int offset);

void clipboard_clear(clipboard_t *clipboard);
void clipboard_copy_selection(clipboard_t *clipboard, lines_t *lines, size_t start, size_t end);
void clipboard_paste_below(clipboard_t *clipboard, lines_t *lines, size_t index);

#endif
