/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "directory_browser.h"
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool is_valid_file(const char *path)
{
    // These are "fake" symlinks. Relative directories should not be displayed!
    return strcmp(path, ".") && strcmp(path, "..");
}

directory_t* directory_scan(const char *path)
{
    size_t total_files = 0;

    // First we need to find out the total number of files
    DIR *system_dir = opendir(path);
    struct dirent *entry;

    if (!system_dir)
        die("failed to open the requested directory");

    while ((entry = readdir(system_dir)) != NULL)
    {
        if (is_valid_file(entry->d_name))
        {
            total_files++;
        }
    }

    // If the directory is empty, just return NULL to indicate an error
    if (total_files == 0)
    {
        closedir(system_dir);

        return NULL;
    }

    directory_t *directory = malloc(sizeof(directory_t));

    if (!directory)
        die("failed to allocate memory for directory struct");

    strcpy(directory->path, path);
    directory->total_files = total_files;

    // Collecting the data by first allocating the needed size
    directory->entries = malloc(directory->total_files * sizeof(entry_t));
    
    if (!directory->entries)
        die("failed to allocate memory for directory entries");

    rewinddir(system_dir);
    size_t i = 0;

    while ((entry = readdir(system_dir)) != NULL)
    {
        if (is_valid_file(entry->d_name))
        {
            // Collecting the type of the file and more
            struct stat st;
            stat(entry->d_name, &st);

            strcpy(directory->entries[i].name, entry->d_name);
            directory->entries[i].is_directory = S_ISDIR(st.st_mode);

            i++;
        }
    }

    closedir(system_dir);

    return directory;
}

void directory_destroy(directory_t *directory)
{
    free(directory->entries);
    free(directory);
}

char* directory_get_file_path(directory_t *dir, size_t file_index)
{
    // This is kept static so it shouldn't be used as an actual value.
    // It should rather be immediately copied to a safe string
    static char file_path[MAX_PATH_LEN];

    // TODO: Make this cross platform maybe. Or maybe not?
    sprintf(file_path, "%s/%s", dir->path, dir->entries[file_index].name);

    return file_path;
}
