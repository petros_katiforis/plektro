/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _INPUT_H
#define _INPUT_H

#include "config.h"

/*
 * Struct that abstracts command input collecting
 * I tried to abstract this to include lines too, but it wasn't worth it
 */
typedef struct
{
    char content[MAX_COMMAND_LEN];
    
    size_t length;
    size_t cursor_x;
} input_t;

/*
 * Returns true if the line has to be cleared
 * (That is when the length is decreasing, e.g. from a backspace)
 */
bool input_handle_input(input_t *input, int c);

void input_clear(input_t *input);

#endif
