/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <ncurses.h>
#include <stdlib.h>

#include "lines.h"
#include "input.h"
#include "directory_browser.h"
#include "config.h"

/*
 * Plektro is a minimal text editor built on and for the terminal
 *
 * Partly inspired by vim and created by Petros Katiforis (Pioyi)
 */

typedef enum
{
    MODE_NORMAL,
    MODE_INSERT,
    MODE_VISUAL
} mode_e;

typedef struct
{
    char file_path[MAX_PATH_LEN];
    char cwd[MAX_PATH_LEN];

    size_t width, height;
    size_t line_index, scroll_offset;

    // The cursor_x variable stores the current x position relative to the start of the lines
    // The editor_x can be larger than the current line but it's necessary for a better UX
    // Note that when you scroll down in vim, the cursor *tries* to follow a vertical line, that's the editor_x
    size_t cursor_x, editor_x;

    // Visual mode data
    size_t selection_start, selection_end;

    mode_e mode;
    lines_t lines;
    clipboard_t clipboard;
    input_t input;

    // Data for the file browser
    WINDOW *browser_win;
} editor_t;

// Global editor instance
editor_t editor;

/*
 * Updates line_index and makes sure the cursor does not overflow
 * It should be kept below the length of the new active line
 */
void editor_set_line_index(size_t index)
{
    editor.line_index = index;
    editor.cursor_x = MIN(editor.lines.data[index].length, editor.editor_x);
}

void editor_load_file(const char *file_path)
{
    lines_init_from_file(&editor.lines, file_path);
    strcpy(editor.file_path, file_path);

    editor_set_line_index(0);
    editor.scroll_offset = 0;
}

void editor_set_cursor_x(size_t value)
{
    editor.cursor_x = value;
    editor.editor_x = value;
}

/*
 * Returns true if the line_index is currently visible on the editor
 */
bool editor_is_on_view(size_t line_index)
{
    return line_index - editor.scroll_offset < editor.height - 2;
}

void editor_create_from_file(const char *file_path)
{
    getmaxyx(stdscr, editor.height, editor.width);

    if (editor.width < MAX_LINE_LEN + 4)
    {
        die("your terminal is too small to display the file. increase the width");
    }

    editor_load_file(file_path);
    editor_set_cursor_x(0);

    // Creating the file browser window based on the terminal's size
    editor.browser_win = newwin(BROWSER_HEIGHT, BROWSER_WIDTH, 1, editor.width - BROWSER_WIDTH - 1);

    getcwd(editor.cwd, MAX_PATH_LEN);
}

/*
 * Clears specified line at y pos
 */
void editor_clear_line(size_t y_pos)
{
    move(y_pos, 0);
    clrtoeol();
}

/*
 * Save state of editor to current file
 */
void editor_save(void)
{
    FILE *file = fopen(editor.file_path, "w");

    if (!file)
    {
        die("failed to open file in write mode");
    }

    for (int i = 0; i < editor.lines.length; i++)
    {
        // Note that we have to insert the new line character ourselves
        fprintf(file, "%s\n", editor.lines.data[i].content);
    }

    fclose(file);
}

/*
 * A general purspose function that should be called whenever the program wants to fetch input
 * It will handle some generic cases that should be checked both when editing and when being prompted
 */
int editor_get_input(void)
{
    int c = getch();

    // Handling common cases
    switch (c)
    {
        case KEY_RESIZE:
            getmaxyx(stdscr, editor.height, editor.width);
            mvwin(editor.browser_win, 1, editor.width - BROWSER_WIDTH - 1);
            
            clear();

            break;
    }

    return c;
}

/*
 * Setting the status will just print a message at the last row of the screen
 * No state is saved because it's not necessary
 */
void editor_set_status(const char *message)
{
    WRAP_WITH_ATTR(COLOR_PAIR(2) | A_BOLD, mvprintw(editor.height - 1, 0, "{status}: %s", message));
}

/*
 * Cursor movement
 * Will also take care of the scroll offset
 */
void editor_move_cursor_down(void)
{
    if (editor.line_index == editor.lines.length - 1) return;

    // If we've got no space, change the scroll offset and refresh the wholes screen
    if (!editor_is_on_view(editor.line_index + 1))
    {
        editor.scroll_offset++;
        clear();
    }

    editor_set_line_index(editor.line_index + 1);
}

void editor_move_cursor_up(void)
{
    if (editor.line_index == 0) return;

    // Pretty much the same logic here
    if (!editor_is_on_view(editor.line_index - 1))
    {
        editor.scroll_offset--;
        clear();
    }

    editor_set_line_index(editor.line_index - 1);
}

/*
 * Starts input mode and freezes editor functionality. Sort of like a prompt
 */
void editor_collect_command_input(void)
{
    input_clear(&editor.input);

    int c = 0;

    // Will finish once the user has pressed enter and the result will be saved at editor.input
    do
    {
        if (c && input_handle_input(&editor.input, c))
        {
            editor_clear_line(editor.height - 1);
        }

        mvprintw(editor.height - 1, 0, "{input}: %s", editor.input.content);
        move(editor.height - 1, 9 + editor.input.cursor_x);

        // Apply changes without rerendering the whole editor
        refresh();
    }
    while ((c = editor_get_input()) != ENTER_KEY);
        
    editor_clear_line(editor.height - 1);
}

/*
 * Editor commands will be implemented here
 * All commands should first collect input from the status bar box, parse it and then execute their code
 */
void editor_process_goto(void)
{
    editor_collect_command_input();

    int line_number = atoi(editor.input.content);

    editor_set_line_index(MIN(editor.lines.length - 1, line_number - 1));
    editor.scroll_offset = editor.line_index;

    clear();
}

size_t editor_get_next_word_end(void)
{
    line_t *cur_line = &editor.lines.data[editor.line_index];

    size_t next_word_index = editor.cursor_x;

    // We first need to skip trailing spaces
    while (cur_line->content[next_word_index] == ' ' 
        && next_word_index < cur_line->length) next_word_index++;

    // We then skip the actual next word
    while (cur_line->content[next_word_index] != ' '
        && next_word_index < cur_line->length) next_word_index++;

    // Finally, just like vim, we want to increment the end position by one and return it
    return next_word_index != cur_line->length ? next_word_index + 1 : next_word_index;

}

void editor_move_one_word(void)
{
    line_t *cur_line = &editor.lines.data[editor.line_index];

    // If we've reached the end of the line, move the cursor down and go to the other
    if (editor.cursor_x == cur_line->length)
    {
        if (editor.line_index != editor.lines.length - 1)
        {
            editor_move_cursor_down();
            editor_set_cursor_x(0);
        }

        return;
    }

    editor_set_cursor_x(editor_get_next_word_end());
}

void editor_delete_next_word(void)
{
    line_t *cur_line = &editor.lines.data[editor.line_index];
    size_t next_word_index = editor_get_next_word_end();
    
    memmove(
        &cur_line->content[editor.cursor_x],
        &cur_line->content[next_word_index],
        cur_line->length - next_word_index + 1 
    );

    cur_line->length -= next_word_index - editor.cursor_x;
    editor_clear_line(editor.line_index - editor.scroll_offset);
}

void editor_move_back_one_word(void)
{
    line_t *cur_line = &editor.lines.data[editor.line_index];

    // If we're at the start of the line move back if possible
    if (editor.cursor_x == 0)
    {
        if (editor.line_index != 0)
        {
            editor_move_cursor_up();
            editor_set_cursor_x(editor.lines.data[editor.line_index].length);
        }

        return;
    }

    // Otherwise, skip all spaces and then the previous word
    while (cur_line->content[editor.cursor_x - 1] == ' ' 
        && editor.cursor_x > 0) editor.cursor_x--;

    while (cur_line->content[editor.cursor_x - 1] != ' '
        && editor.cursor_x > 0) editor.cursor_x--;
}

/*
 * Deletes all lines in the specified range and moves lines accordingly
 */
void editor_delete_lines(size_t start, size_t end)
{
    size_t selection_end = MAX(start, end);
    size_t selection_start = MIN(start, end);
    size_t total_lines = selection_end - selection_start + 1;

    // Moving all lines a place back will delete the current one
    lines_move_from_by(&editor.lines, selection_end + 1, -total_lines);

    // Check if the cursor has moved out of the lines
    editor_set_line_index(MIN(editor.lines.length - 1, selection_start));

    // Make sure the editor scrolls if the line is not visible for any reason
    if (!editor_is_on_view(editor.line_index))
    {
        editor.scroll_offset = editor.line_index;
    }

    // Check if we've deleted all the lines
    if (editor.lines.length == 0)
    {
        line_clear(&editor.lines.data[0]);
        editor_set_cursor_x(0);

        editor.lines.length++;
    }
    
    clear();
}

/*
 * Prompts the file browser and freezes the normal usage
 * Will change the file based on the user selection
 */
void editor_prompt_file_browser(void)
{
    wclear(editor.browser_win);

    int c = 0;
    size_t selected_index = 0;

    // Disabling the cursor for this mode and scanning the directory
    curs_set(0);
    directory_t *dir = directory_scan(editor.cwd);

    do
    {
        // Handle some movement here
        switch (c)
        {
            case UP_KEY:
                selected_index = selected_index > 0 ? selected_index - 1 : dir->total_files - 1;

                break;

            case DOWN_KEY:
                selected_index = (selected_index + 1) % dir->total_files;

                break;

            // File selection
            case ENTER_KEY:
                if (!dir) break;

                // If it's a directory, update the UI and show its content instead
                if (dir->entries[selected_index].is_directory)
                {
                    char *new_path = directory_get_file_path(dir, selected_index);

                    directory_destroy(dir);

                    dir = directory_scan(new_path);
                    selected_index = 0;
                }
                // If it's a file, just open it
                // I used to save the old file by force here, but I decided against it later on
                else
                {
                    editor_load_file(directory_get_file_path(dir, selected_index));
                    
                    goto finish_browser_prompt;
                }

                // In both cases, we should clear the window
                wclear(editor.browser_win);

                break;
        }

        // Scrolling support for larger directories
        size_t page_number = selected_index / (BROWSER_HEIGHT - 2);

        // If the directory is empty let the user know
        // In this case he should just quit using the EXIT_KEY
        if (!dir)
        {
            mvwaddstr(editor.browser_win, 1, 1, "(directory is empty)");    
        }
        else
        {
            for (int i = 0; i < MIN(dir->total_files, BROWSER_HEIGHT - 2); i++)
            {
                size_t index = i + page_number * (BROWSER_HEIGHT - 2);

                // Special effect for selected item
                if (index == selected_index) wattron(editor.browser_win, A_STANDOUT);
                if (dir->entries[index].is_directory) wattron(editor.browser_win, A_BOLD);

                mvwprintw(editor.browser_win, i + 1, 1, " %-*s", BROWSER_WIDTH - 2, dir->entries[index].name);

                wattroff(editor.browser_win, A_STANDOUT | A_BOLD);
            }
        }
    
        box(editor.browser_win, 0, 0);
        wrefresh(editor.browser_win);

    } while ((c = editor_get_input()) != EXIT_KEY);

finish_browser_prompt:
    clear();

    curs_set(1);

    if (dir)
    {
        directory_destroy(dir);
    }
}

/*
 * Handle string input on the current line 
 */
void editor_handle_string_input(int c)
{
    line_t *cur_line = &editor.lines.data[editor.line_index];
    line_t *prev_line = cur_line - 1;
    line_t *next_line = cur_line + 1;

    switch (c)
    {
        // Convert all incoming tabs to spaces
        case TAB_KEY:
            if (cur_line->length > MAX_LINE_LEN - SPACES_PER_TAB) return;

            memmove(
                &cur_line->content[editor.cursor_x + SPACES_PER_TAB],
                &cur_line->content[editor.cursor_x],
                cur_line->length - editor.cursor_x + 1
            );

            memset(&cur_line->content[editor.cursor_x], ' ', SPACES_PER_TAB);

            editor_set_cursor_x(editor.cursor_x + SPACES_PER_TAB);
            cur_line->length += SPACES_PER_TAB;

            break;

        // The enter key while in insert mode should cause a new line to be created
        case ENTER_KEY:
            lines_move_from_by(&editor.lines, editor.line_index + 1, 1);

            strcpy(next_line->content, cur_line->content + editor.cursor_x);
            next_line->length = cur_line->length - editor.cursor_x;

            cur_line->content[editor.cursor_x] = 0;
            cur_line->length = editor.cursor_x;

            editor_move_cursor_down();
            editor_set_cursor_x(0);
            clear();

            break;
    }

    if (c == KEY_BACKSPACE)
    {
        // Handle case where the backspace is being pressed at the start of the line
        if (c == KEY_BACKSPACE && editor.cursor_x == 0)
        {
            // We can't delete the only line
            if (editor.line_index == 0) return;

            // Check if the tabs fit
            if (cur_line->length + prev_line->length > MIN(editor.width - 4, MAX_LINE_LEN)) return;

            if (cur_line->length > 0)
            {
                strcpy(&prev_line->content[prev_line->length], cur_line->content);
            }

            int new_cursor_x = prev_line->length;

            prev_line->length += cur_line->length;

            lines_move_from_by(&editor.lines, editor.line_index + 1, -1);
            editor_move_cursor_up();
            editor_set_cursor_x(new_cursor_x);
                
            clear();
        }
        else if (editor.cursor_x > 0)
        {
            memmove(
                &cur_line->content[editor.cursor_x - 1],
                &cur_line->content[editor.cursor_x],
                cur_line->length - editor.cursor_x + 1
            );

            cur_line->length--;
            editor.cursor_x--;

            editor_clear_line(editor.line_index - editor.scroll_offset);
        }
    }
    // Check if the character is printable first
    else if (isprint(c) && cur_line->length < MAX_LINE_LEN)
    {
        memmove(
            &cur_line->content[editor.cursor_x + 1],
            &cur_line->content[editor.cursor_x],
            cur_line->length - editor.cursor_x + 1
        );

        cur_line->content[editor.cursor_x] = c;
        cur_line->length++;
        editor_set_cursor_x(editor.cursor_x + 1);

        return;
    }
}

/*
 * Processes input event and returns the character for further action
 */
int editor_handle_input()
{
    int c = editor_get_input();

    // Always clean the status on next key press
    editor_clear_line(editor.height - 1);

    line_t *cur_line = &editor.lines.data[editor.line_index];
    line_t *prev_line = cur_line - 1;
    line_t *next_line = cur_line + 1;

    // The switch mode key is special
    if (c == MODE_KEY)
    {
        // Only these two modes allow for instant switching
        if (editor.mode == MODE_NORMAL) editor.mode = MODE_INSERT;
        else if (editor.mode == MODE_INSERT) editor.mode = MODE_NORMAL;

        return c;
    }

    // Some common keys between all modes
    if (editor.mode == MODE_NORMAL || editor.mode == MODE_INSERT || editor.mode == MODE_VISUAL)
    {
        switch (c)
        {
            case DOWN_KEY:
                editor_move_cursor_down();
                
                break;

            case UP_KEY:
                editor_move_cursor_up();

                break;

            /*
             * Handling horizontal movement
             * It's to be implemented an all current modes
             */
            case KEY_RIGHT:
                if (editor.cursor_x < cur_line->length)
                {
                    editor_set_cursor_x(editor.cursor_x + 1);
                }

                break;

            case KEY_LEFT:
                if (editor.cursor_x > 0)
                {
                    editor_set_cursor_x(editor.cursor_x - 1);
                }

                break;
        }
    }

    if (editor.mode == MODE_VISUAL)
    {
        switch (c)
        {
            case VISUAL_MODE_KEY:
                editor.mode = MODE_NORMAL;

                break;

            // Visual mode will support deleting, cutting and yanking
            case DELETE_LINE_KEY:
                editor_delete_lines(editor.selection_start, editor.selection_end);
                editor.mode = MODE_NORMAL;

                break;

            case YANK_KEY:
                clipboard_copy_selection(&editor.clipboard, &editor.lines, editor.selection_start, editor.selection_end);
                editor.mode = MODE_NORMAL;

                break;

            case CUT_LINE_KEY:
                clipboard_copy_selection(&editor.clipboard, &editor.lines, editor.selection_start, editor.selection_end);
                editor_delete_lines(editor.selection_start, editor.selection_end);
                editor.mode = MODE_NORMAL;

                break;
        }

        // Update selection to make sure it's up to date
        editor.selection_end = editor.line_index;
    }
    else if (editor.mode == MODE_NORMAL)
    {
        switch (c)
        {
            case VISUAL_MODE_KEY:
                editor.mode = MODE_VISUAL;
                editor.selection_start = editor.selection_end = editor.line_index;
                
                break;

            case SAVE_KEY:
                editor_save();
                editor_set_status("the file was successfully saved!");

                break;

            case DELETE_LINE_KEY:
                // Pass a range of one item to delete the current line.
                // These functions are written this way to support the deleting of multiple lines as well
                editor_delete_lines(editor.line_index, editor.line_index);

                break;

            case DELETE_WORD_KEY:
                editor_delete_next_word();                

                break;

            case MOVE_WORD_KEY:
                editor_move_one_word();

                break;

            case MOVE_WORD_BACK_KEY:
                editor_move_back_one_word();

                break;

            // Yanking means copying the line. It's vim terminology
            case YANK_KEY:
                clipboard_copy_selection(&editor.clipboard, &editor.lines, editor.line_index, editor.line_index);

                break;

            case CUT_LINE_KEY:
                clipboard_copy_selection(&editor.clipboard, &editor.lines, editor.line_index, editor.line_index);
                editor_delete_lines(editor.line_index, editor.line_index);

                break;

            // Pasting lines below and up relative to current index using the clipboard
            case PASTE_BELOW_KEY:
                clipboard_paste_below(&editor.clipboard, &editor.lines, editor.line_index);
                editor_move_cursor_down();
                clear();

                break;

            case PASTE_UP_KEY:
                clipboard_paste_below(&editor.clipboard, &editor.lines, editor.line_index - 1);
                clear();

                break;

            // These keys create a new line and then switch to insert mode
            // They work exactly like vim
            case INSERT_BELOW_KEY:
                lines_move_from_by(&editor.lines, editor.line_index + 1, 1);

                editor.mode = MODE_INSERT;
                editor_move_cursor_down();
                clear();

                break;
            
            case INSERT_UP_KEY:
                lines_move_from_by(&editor.lines, editor.line_index, 1);

                editor.mode = MODE_INSERT;
                clear();

                break;

            // Handling commands
            case GOTO_KEY:
                editor_process_goto();

                break;

            case CHANGE_FILE_KEY:
                editor_prompt_file_browser();

                break;
        }
    }
    else if (editor.mode == MODE_INSERT)
    {
        editor_handle_string_input(c);
    }

    return c;
}

/*
 * Some custom colors
 * They must all be greated than 16, that is COLOR_RED
 */
enum
{
    COLOR_LINES = 16,
    COLOR_STATUS,
    COLOR_SELECT
};

int main(int argc, char **argv)
{
    if (argc < 2)
        die("please insert a valid filename to start editing!");

    // Trying to enable colors on linux
    putenv("TERM=xterm-256color");

    initscr();

    keypad(stdscr, true);
    start_color();
    curs_set(1);
    cbreak();
    noecho();
    raw();

    if (!has_colors() || !can_change_color())
        die("your terminal doesn't support custom colors!");

    editor_create_from_file(argv[1]);

    // Creating some colors, you can customize this to your likings
    init_color(COLOR_LINES, 600, 900, 600);
    init_color(COLOR_STATUS, 400, 600, 900);
    init_color(COLOR_SELECT, 900, 600, 900);

    init_pair(1, COLOR_LINES, COLOR_BLACK);
    init_pair(2, COLOR_STATUS, COLOR_BLACK);
    init_pair(3, COLOR_SELECT, COLOR_BLACK);

    // Rendering and input loop
    do
    {
        // Rendering the lines
        for (int i = 0; i < MIN(editor.height - 2, editor.lines.length - editor.scroll_offset); i++)
        {
            size_t line_index = i + editor.scroll_offset;
            
            // Checking if the line is selected. Note that editor.selection_end is not always the largest value
            size_t end = MAX(editor.selection_start, editor.selection_end);
            size_t start = MIN(editor.selection_start, editor.selection_end);
            bool is_selected = (editor.mode == MODE_VISUAL && line_index >= start && line_index <= end);

            int flags = is_selected ? (COLOR_PAIR(3) | A_BOLD) : COLOR_PAIR(1);

            // Printing line number followed by the line content
            WRAP_WITH_ATTR(flags, mvprintw(i, 0, "%3d ", line_index + 1));

            mvprintw(i, 4, "%s", editor.lines.data[line_index].content);
        }

        // Updating cursor position based on current line
        move(
            editor.line_index - editor.scroll_offset,
            4 + editor.cursor_x
        );

        refresh();

    } while (editor_handle_input() != EXIT_KEY || editor.mode != MODE_NORMAL);

    endwin();
}
