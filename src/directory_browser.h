/* Plektro Editor 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _DIRECTORY_BROWSER_H
#define _DIRECOTRY_BROWSER_H

#include <dirent.h>
#include "config.h"

typedef struct
{
    char name[MAX_PATH_LEN];
    bool is_directory;
} entry_t;
/*
 * Represents an open and analyzed directory that can be printed
 * Will be used recursively to make the directory browser efficient and powerfull
 */
typedef struct
{
    size_t total_files;
    char path[MAX_PATH_LEN];

    entry_t *entries;
} directory_t;

directory_t* directory_scan(const char *path);
void directory_destroy(directory_t *dir);

/*
 * Returns the full path of a valid file index inside the directory
 */
char* directory_get_file_path(directory_t *dir, size_t file_index);

#endif
